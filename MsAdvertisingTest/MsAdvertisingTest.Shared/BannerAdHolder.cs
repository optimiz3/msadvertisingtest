﻿using System;
using System.Globalization;

using Microsoft.Advertising;
using Microsoft.Advertising.WinRT.UI;

using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Input;

using Windows.UI.Xaml.Shapes;

namespace MsAdvertisingTest
{
    internal class BannerAdHolder
    {
        // set this to 1 to test Dispose
        private const int DisposeCount = int.MaxValue;

        private readonly TappedEventHandler adControl_Tapped;

        private readonly EventHandler<object> adControl_LayoutUpdated;

        private readonly EventHandler<RoutedEventArgs> adControl_AdRefreshed;

        private readonly EventHandler<AdErrorEventArgs> adControl_ErrorOccurred;

        private readonly DispatcherTimer timer;

        private readonly Panel parent;

        private readonly AdInfo adInfo;

        private readonly string name;

        private AdControl adControl;

        // draw a red rectangle over any AdControl that is in an error state,
        // or is Visible only because Refresh is being called but failed previously
        // so we can see timeouts
        private Rectangle rectangle;

        private BannerAdHolderState state;

        private Visibility visibility;

        private int count;

        private bool tapped;

        public BannerAdHolder(
            Panel parent,
            AdInfo info,
            string name)
        {
            this.adControl_Tapped =
                this.AdControl_Tapped;

            this.adControl_LayoutUpdated =
                this.AdControl_LayoutUpdated;

            this.adControl_AdRefreshed =
                this.AdControl_AdRefreshed;

            this.adControl_ErrorOccurred =
                this.AdControl_ErrorOccurred;

            var timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(30),
            };

            timer.Tick += this.Timer_Tick;

            this.timer = timer;

            this.parent = parent;

            this.adInfo = info;

            this.name = name;
        }

        public Visibility Visiblity
        {
            get { return this.visibility; }
            set
            {
                this.visibility = value;

                var adControl = this.adControl;

                if (adControl != null)
                {
                    adControl.Visibility = value;
                }
            }
        }

        public void Refresh()
        {
            if (this.timer.IsEnabled)
            {
                throw new InvalidOperationException();
            }

            if (state != BannerAdHolderState.None)
            {
                throw new InvalidOperationException();
            }

            var adControl = this.adControl;

            if (adControl == null)
            {
                this.CreateControl();

                adControl = this.adControl;
            }

            this.state = BannerAdHolderState.Pending;

            this.timer.Start();

            if (adControl.Visibility !=
                    Visibility.Visible)
            {
                // first time visibility is set to visible trigger refresh
                adControl.Visibility =
                    Visibility.Visible;

                return;
            }

            this.rectangle.Visibility =
                Visibility.Collapsed;

            // control may have been hidden from prior failure
            adControl.Visibility =
                Visibility.Visible;

            var timestamp = DateTime.UtcNow;

            DebugHelper.WriteLine(
                "{0:yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffff} AdControl_Refresh(): adUnitId={1}, name={2}",
                timestamp,
                adControl.AdUnitId,
                this.name);

            adControl.Refresh();
        }

        //[Conditional("DEBUG")]
        private static void DebugWriteChildren(
            DependencyObject reference,
            int indent)
        {
            DebugHelper.WriteLine(new string(' ', indent * 4) + reference.ToString());

            var childrenCount = VisualTreeHelper.GetChildrenCount(
                reference);

            for (var i = 0; i != childrenCount; ++i)
            {
                var child = VisualTreeHelper.GetChild(
                    reference,
                    i);

                DebugWriteChildren(child, indent + 1);
            }
        }

        private void CreateControl()
        {
            if (this.adControl != null)
            {
                throw new InvalidOperationException();
            }

            var adControl = new AdControl()
            {
                Visibility = Visibility.Collapsed,
                IsAutoRefreshEnabled = false,
            };

            var adInfo = this.adInfo;

            adControl.ApplicationId =
                adInfo.ApplicationId.ToString("D");

            adControl.AdUnitId =
                adInfo.AdUnitId.ToString(
                    CultureInfo.InvariantCulture);

            adControl.Width = adInfo.Width;

            adControl.Height = adInfo.Height;

            var parent = this.parent;

            var canvas = parent as Canvas;

            if (canvas != null)
            {
                Canvas.SetLeft(adControl, 0);

                Canvas.SetTop(adControl, 0);
            }

            parent.Children.Add(adControl);

            adControl.Tapped +=
                this.adControl_Tapped;

            adControl.LayoutUpdated +=
                this.adControl_LayoutUpdated;

            adControl.AdRefreshed +=
                this.adControl_AdRefreshed;

            adControl.ErrorOccurred +=
                this.adControl_ErrorOccurred;

            this.adControl = adControl;

            var rectangle = new Rectangle()
            {
                Opacity = .6,
                Visibility = Visibility.Collapsed,
            };

            rectangle.Width = adInfo.Width;

            rectangle.Height = adInfo.Height;

            if (canvas != null)
            {
                Canvas.SetLeft(rectangle, 0);

                Canvas.SetTop(rectangle, 0);
            }

            parent.Children.Add(rectangle);

            this.rectangle = rectangle;
        }

        private void OnComplete(FailureMode failureMode)
        {
            if (failureMode == FailureMode.None)
            {
                this.rectangle.Visibility =
                    Visibility.Collapsed;

                return;
            }

            // to make it more apparent when we are waiting due to a failure
            this.adControl.Visibility =
                Visibility.Collapsed;

            switch (failureMode)
            {
                case FailureMode.NoAdAvailable:
                    this.rectangle.Fill =
                        new SolidColorBrush(Colors.Gray);
                    break;

                case FailureMode.OtherError:
                    this.rectangle.Fill =
                        new SolidColorBrush(Colors.Red);
                    break;

                case FailureMode.Timeout:
                    this.rectangle.Fill =
                        new SolidColorBrush(Colors.Orange);
                    break;
            }

            this.rectangle.Visibility =
                Visibility.Visible;
        }

        private void RemoveControl()
        {
            if (this.adControl == null)
            {
                throw new InvalidOperationException();
            }

            var parent = this.parent;

            var adControl = this.adControl;

            this.adControl = null;

            adControl.Tapped -=
                this.adControl_Tapped;

            adControl.LayoutUpdated -=
                this.adControl_LayoutUpdated;

            adControl.AdRefreshed -=
                this.adControl_AdRefreshed;

            adControl.ErrorOccurred -=
                this.adControl_ErrorOccurred;

            parent.Children.Remove(adControl);

            //adControl.Dispose();
            GC.Collect();
        }

        private void HandleComplete()
        {
            this.tapped = false;

            ++this.count;

            if (this.count >= DisposeCount)
            {
                this.RemoveControl();

                this.count = 0;
            }

            this.timer.Start();
        }

        private void AdControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (this.state == BannerAdHolderState.Success)
            {
                this.tapped = true;
            }
        }

        private void AdControl_LayoutUpdated(object sender, object e)
        {
            var parent = this.parent;

            if (adControl != null &&
                adControl.Visibility == Visibility.Visible)
            {
                var popups = VisualTreeHelper.GetOpenPopups(
                    Window.Current);

                foreach (var popup in popups)
                {
                    if (!this.tapped ||
                        popup.Width != parent.Width ||
                        popup.Height != parent.Height)
                    {
                        //popup.IsOpen = false;
                    }
                }
            }
        }

        private void AdControl_ErrorOccurred(object sender, AdErrorEventArgs e)
        {
            var adControl = (AdControl)sender;

            if (!adControl.Dispatcher.HasThreadAccess)
            {
                throw new InvalidOperationException();
            }

            this.timer.Stop();

            this.state = BannerAdHolderState.Failure;

            var timestamp = DateTime.UtcNow;

            DebugHelper.WriteLine(
                "{0:yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffff} AdControl_ErrorOccurred: adUnitId={1}, name={2}, errorMessage={3}",
                timestamp,
                adControl.AdUnitId,
                this.name,
                e.ErrorMessage);

            if (e.ErrorCode == ErrorCode.NoAdAvailable)
            {
                this.OnComplete(FailureMode.NoAdAvailable);
            }
            else
            {
                this.OnComplete(FailureMode.OtherError);
            }

            this.HandleComplete();
        }

        private void AdControl_AdRefreshed(object sender, RoutedEventArgs e)
        {
            var adControl = (AdControl)sender;

            if (!adControl.Dispatcher.HasThreadAccess)
            {
                throw new InvalidOperationException();
            }

            this.timer.Stop();

            this.state = BannerAdHolderState.Success;

            var timestamp = DateTime.UtcNow;

            DebugHelper.WriteLine(
                "{0:yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffff} AdControl_AdRefreshed: adUnitId={1}, name={2}",
                timestamp,
                adControl.AdUnitId,
                this.name);

            this.OnComplete(FailureMode.None);

            this.HandleComplete();
        }

        private void Timer_Tick(object sender, object e)
        {
            this.timer.Stop();

            var state = this.state;

            if (state == BannerAdHolderState.Pending)
            {
                var timestamp = DateTime.UtcNow;

                DebugHelper.WriteLine(
                    "{0:yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffff} Timer_Tick: adUnitId={1}, name={2}, errorMessage=Timeout",
                    timestamp,
                    this.adControl.AdUnitId,
                    this.name);

                this.OnComplete(FailureMode.Timeout);
            }

            this.state = BannerAdHolderState.None;

            this.Refresh();
        }

        private enum FailureMode
        {
            None,
            NoAdAvailable,
            OtherError,
            Timeout,
        }

        private enum BannerAdHolderState
        {
            None,
            Pending,
            Success,
            Failure,
        }
    }
}
