﻿using System;

namespace MsAdvertisingTest
{
    internal class AdInfo
    {
        // NOTE: replace these with live IDs to test the sandbox breakout bug
        // https://msdn.microsoft.com/en-us/library/mt313178%28v=msads.30%29.aspx
        private static readonly Guid DesktopApplicationId =
            Guid.ParseExact("d25517cb-12d4-4699-8bdc-52040c712cab", "D");

        private static readonly Guid MobileApplicationId =
            Guid.ParseExact("3f83fe91-d6be-434d-a0ae-7351c5a997f1", "D");

        public static readonly AdInfo Desktop300x250 = new AdInfo
        {
            ApplicationId = DesktopApplicationId,
            AdUnitId = 10043121,
            Width = 300,
            Height = 250,
        };

        public static readonly AdInfo Desktop300x600 = new AdInfo
        {
            ApplicationId = DesktopApplicationId,
            AdUnitId = 10043122,
            Width = 300,
            Height = 600,
        };

        public static readonly AdInfo Desktop728x90 = new AdInfo
        {
            ApplicationId = DesktopApplicationId,
            AdUnitId = 10043123,
            Width = 728,
            Height = 90,
        };

        public static readonly AdInfo Mobile320x50 = new AdInfo
        {
            ApplicationId = MobileApplicationId,
            AdUnitId = 10865270,
            Width = 320,
            Height = 50,
        };

        public static readonly AdInfo Mobile480x80 = new AdInfo
        {
            ApplicationId = MobileApplicationId,
            AdUnitId = 10865272,
            Width = 480,
            Height = 80,
        };

        public static readonly AdInfo Mobile640x100 = new AdInfo
        {
            ApplicationId = MobileApplicationId,
            AdUnitId = 10865273,
            Width = 640,
            Height = 100,
        };

        public static readonly AdInfo Mobile300x50 = new AdInfo
        {
            ApplicationId = MobileApplicationId,
            AdUnitId = 10865275,
            Width = 300,
            Height = 50,
        };

        public static readonly AdInfo Interstitial = new AdInfo
        {
            ApplicationId = DesktopApplicationId,
            AdUnitId = 11389925,
        };

        public Guid ApplicationId
        {
            get; private set;
        }

        public int AdUnitId
        {
            get; private set;
        }

        public int Width
        {
            get; private set;
        }

        public int Height
        {
            get; private set;
        }
    }
}
