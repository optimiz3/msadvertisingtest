﻿using System;
using System.Diagnostics;
using System.Globalization;

using Microsoft.Advertising.WinRT.UI;

namespace MsAdvertisingTest
{
    internal class InterstitialAdHolder
    {
        private readonly EventHandler<object> interstitialAd_AdReady;

        private readonly EventHandler<AdErrorEventArgs> interstitialAd_ErrorOccurred;

        private readonly EventHandler<object> interstitialAd_Completed;

        private readonly EventHandler<object> interstitialAd_Cancelled;

        private readonly AdInfo adInfo;

        private InterstitialAd interstitialAd;

        public event EventHandler<object> Completed;

        public InterstitialAdHolder(
            AdInfo adInfo)
        {
            this.interstitialAd_AdReady =
                this.InterstitialAd_AdReady;

            this.interstitialAd_ErrorOccurred =
                this.InterstitialAd_ErrorOccurred;

            this.interstitialAd_Completed =
                this.InterstitialAd_Completed;

            this.interstitialAd_Cancelled =
                this.InterstitialAd_Cancelled;

            this.adInfo = adInfo;
        }

        public void Show()
        {
            if (this.interstitialAd == null)
            {
                this.CreateAdControl();
            }

            var applicationId =
                adInfo.ApplicationId.ToString("D");

            var adUnitId =
                adInfo.AdUnitId.ToString(
                    CultureInfo.InvariantCulture);

            this.interstitialAd.RequestAd(
                AdType.Video,
                applicationId,
                adUnitId);
        }

        private void CreateAdControl()
        {
            if (this.interstitialAd != null)
            {
                throw new InvalidOperationException();
            }

            var interstitialAd = new InterstitialAd();

            interstitialAd.AdReady +=
                this.interstitialAd_AdReady;

            interstitialAd.ErrorOccurred +=
                this.interstitialAd_ErrorOccurred;

            interstitialAd.Completed +=
                this.interstitialAd_Completed;

            interstitialAd.Cancelled +=
                this.interstitialAd_Cancelled;

            this.interstitialAd = interstitialAd;
        }

        private void InterstitialAd_AdReady(
            object sender,
            object e)
        {
            this.interstitialAd.Show();
        }

        private void InterstitialAd_ErrorOccurred(
            object sender,
            AdErrorEventArgs e)
        {
            Debug.WriteLine("Interstitial: " + e.ErrorMessage);

            var eventHandler = this.Completed;

            if (eventHandler != null)
            {
                eventHandler(null, e.ErrorMessage);
            }
        }

        private void InterstitialAd_Completed(
            object sender,
            object e)
        {
            Debug.WriteLine("Interstitial: Completed");

            var eventHandler = this.Completed;

            if (eventHandler != null)
            {
                eventHandler(null, true);
            }
        }

        private void InterstitialAd_Cancelled(
            object sender,
            object e)
        {
            Debug.WriteLine("Interstitial: Cancelled");

            var eventHandler = this.Completed;

            if (eventHandler != null)
            {
                eventHandler(null, false);
            }
        }
    }
}
