﻿using System;

namespace MsAdvertisingTest
{
    internal static class DebugHelper
    {
        public static void WriteLine(
            string format,
            params object[] args)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine(
                format,
                args);
#else
            // so we can see debug output in Release mode;
            // requires native debugging
            var str = string.Format(format, args) +
                Environment.NewLine;

            SafeNativeMethods.OutputDebugString(
            str);
#endif
        }
    }
}
