﻿using System;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MsAdvertisingTest
{
    public sealed partial class MainPage : Page
    {
        private readonly BannerAdHolder[] adHolders;

        private readonly InterstitialAdHolder interstitialAdHolder;

        public MainPage()
        {
            this.InitializeComponent();

            BannerAdHolder[] bannerAdHolders;

            bool mobile;

#if !WINDOWS_PHONE_APP
            mobile = false;
#else
            mobile = true;
#endif
            if (!mobile)
            {
                bannerAdHolders = new BannerAdHolder[]
                {
                    new BannerAdHolder(
                        this.adControlDesktop728x90,
                        AdInfo.Desktop728x90,
                        "Header"),
                    new BannerAdHolder(
                        this.adControlDesktopLeft1,
                        AdInfo.Desktop300x250,
                        "TopLeft"),
                    new BannerAdHolder(
                        this.adControlDesktopRight1,
                        AdInfo.Desktop300x250,
                        "TopRight"),
                    new BannerAdHolder(
                        this.adControlDesktopLeft2,
                        AdInfo.Desktop300x250,
                        "BottomLeft"),
                    new BannerAdHolder(
                        this.adControlDesktopRight2,
                        AdInfo.Desktop300x250,
                        "BottomRight"),
                };

                this.stackPanelDesktop.Visibility =
                    Visibility.Visible;
            }
            else
            {
                const int MobileAdCount = 10;

                var mobileAdInfo = GetMobileAdInfo();

                bannerAdHolders = new BannerAdHolder[MobileAdCount];

                for (var i = 0; i != bannerAdHolders.Length; ++i)
                {
                    var canvas = new Canvas
                    {
                        Width = mobileAdInfo.Width,
                        Height = mobileAdInfo.Height,
                    };

                    this.stackPanelMobile.Children.Add(canvas);

                    bannerAdHolders[i] = new BannerAdHolder(
                        canvas,
                        mobileAdInfo,
                        "Banner" + i.ToString());
                };

                this.stackPanelMobile.Visibility =
                    Visibility.Visible;
            }

            this.adHolders = bannerAdHolders;

            var interstitialAdHolder = new InterstitialAdHolder(
                AdInfo.Interstitial);

            interstitialAdHolder.Completed +=
                this.InterstitialAdHolder_Completed;

            this.interstitialAdHolder = interstitialAdHolder;

            this.Loaded += this.Page_Loaded;
        }

        private static AdInfo GetMobileAdInfo()
        {
            var windowWidth =
                Window.Current.Bounds.Width;

            if (windowWidth >= AdInfo.Mobile640x100.Width)
            {
                return AdInfo.Mobile640x100;
            }
            else if (windowWidth >= AdInfo.Mobile480x80.Width)
            {
                return AdInfo.Mobile480x80;
            }
            else if (windowWidth >= AdInfo.Mobile320x50.Width)
            {
                return AdInfo.Mobile320x50;
            }

            return AdInfo.Mobile300x50;
        }

        private static void SetElementSize(
            FrameworkElement element,
            AdInfo adInfo)
        {
            element.Width = adInfo.Width;

            element.Height = adInfo.Height;
        }

        private void InterstitialAdHolder_Completed(
            object sender,
            object e)
        {
            this.buttonInterstitial.IsEnabled = true;

            foreach (var adHolder in this.adHolders)
            {
                adHolder.Visiblity = Visibility.Visible;
            }
        }

        private void Page_Loaded(
            object sender,
            RoutedEventArgs e)
        {
            for (var i = 0; i != adHolders.Length; ++i)
            {
                var adHolder = adHolders[i];

                adHolder.Refresh();
            }
        }

        private void Button_Click(
            object sender,
            RoutedEventArgs e)
        {
            this.buttonInterstitial.IsEnabled = false;

            foreach (var adHolder in this.adHolders)
            {
                adHolder.Visiblity = Visibility.Collapsed;
            }

            this.interstitialAdHolder.Show();
        }
    }
}
