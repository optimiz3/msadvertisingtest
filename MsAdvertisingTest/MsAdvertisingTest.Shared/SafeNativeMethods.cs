﻿using System.Runtime.InteropServices;

namespace MsAdvertisingTest
{
    internal static class SafeNativeMethods
    {
        [DllImport(
            "kernel32.dll",
            EntryPoint = "OutputDebugStringA",
            ExactSpelling = true)]
        internal static extern void OutputDebugString(
            [MarshalAs(UnmanagedType.LPStr)]
            string outputString);
    }
}
